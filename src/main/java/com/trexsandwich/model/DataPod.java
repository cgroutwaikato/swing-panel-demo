package com.trexsandwich.model;

import java.util.Objects;

/**
 * An example object encapsulating information that is passed between panels
 */
public class DataPod {
    String data;

    public DataPod(final String initial) {
        this.data = Objects.requireNonNullElse(initial, "");
    }

    public DataPod() {
        this("");
    }

    public String getData() {
        return data;
    }

    public void setData(final String data) {
        this.data = Objects.requireNonNullElse(data, "");
    }
}
