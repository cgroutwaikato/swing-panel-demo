package com.trexsandwich;

import com.trexsandwich.panels.AlicePanel;

import javax.swing.*;

public class Main {
    public static void main(final String[] args) {
        SwingUtilities.invokeLater(() -> {
            final ExampleFrame efs = new ExampleFrame();

            efs.panelPush(new AlicePanel());
        });
    }
}
