package com.trexsandwich.panels;

import com.trexsandwich.interfaces.StackAwareFrame;
import com.trexsandwich.interfaces.StackAwarePanel;
import com.trexsandwich.model.DataPod;

import javax.swing.*;
import java.awt.*;

public class BobPanel extends JPanel implements StackAwarePanel {
    private final DataPod pod;

    private StackAwareFrame host;

    public BobPanel(final DataPod d) {
        this.pod = d;
        this.setBackground(Color.ORANGE);

        // BobPanel child components are created immediately

        final JLabel jl = new JLabel(this.pod.getData());
        this.add(jl);

        final JButton jb = new JButton("Back");
        this.add(jb);

        jb.addActionListener(e -> {
            this.host.panelPop(false);
        });
    }

    @Override
    public boolean isSAPResizable() {
        return false;
    }

    @Override
    public String getSAPTitle() {
        return "Bob";
    }

    @Override
    public JPanel getPanel() {
        return this;
    }

    @Override
    public void activate(final StackAwareFrame host) {
        this.host = host;
    }

    @Override
    public void deactivate(final StackAwareFrame host) {
        this.host = null;
    }
}
