package com.trexsandwich.panels;

import com.trexsandwich.interfaces.StackAwareFrame;
import com.trexsandwich.interfaces.StackAwarePanel;
import com.trexsandwich.model.DataPod;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;

public class AlicePanel extends JPanel implements StackAwarePanel {
    boolean initialised = false;
    JTextField textField;
    JButton bobButton;
    JButton exitButton;
    private StackAwareFrame host;

    public AlicePanel() {
        this.setBackground(Color.green);
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    }


    private void init() {
        textField = new JTextField(30);
        bobButton = new JButton("Open Bob Panel");
        exitButton = new JButton("Exit");

        this.add(textField);
        this.add(bobButton);
        this.add(exitButton);

        textField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(final DocumentEvent e) {
                bobButton.setEnabled(!textField.getText().isEmpty());
            }

            @Override
            public void removeUpdate(final DocumentEvent e) {
                bobButton.setEnabled(!textField.getText().isEmpty());
            }

            @Override
            public void changedUpdate(final DocumentEvent e) {
                bobButton.setEnabled(!textField.getText().isEmpty());
            }
        });

        exitButton.addActionListener((e -> host.panelPop(true)));

        bobButton.setEnabled(false);
        bobButton.addActionListener(e -> {
            final StackAwarePanel bob = new BobPanel(new DataPod(textField.getText()));
            host.panelPush(bob);
        });
    }

    @Override
    public boolean isSAPResizable() {
        return true;
    }

    @Override
    public String getSAPTitle() {
        return "Alice";
    }

    @Override
    public JPanel getPanel() {
        return this;
    }

    @Override
    public void activate(final StackAwareFrame host) {
        this.host = host;

        // AlicePanel is initialised lazily - child components are only created when the panel is
        // about to be created. This is not critical for this sort of simple panel, but can be good
        // practice for larger, more complex controls.
        if (!initialised) {
            this.init();
            initialised = true;
        }
    }


    @Override
    public void deactivate(final StackAwareFrame host) {
        this.host = null;
    }
}
