package com.trexsandwich.interfaces;

/**
 * Defines the behaviour of a {@link javax.swing.JFrame} that maintains an internal stack of {@link javax.swing.JPanel}s that should be displayed in-order.
 */
public interface StackAwareFrame {
    /**
     * <p>Add a new {@link StackAwarePanel} to the internal panel stack as the top-most element.</p>
     *
     * <p>As the top-most panel in the stack should be the active panel, this should result
     * in the pushed {@link StackAwarePanel} being displayed in the frame.</p>
     *
     * @param sap {@link StackAwarePanel} to push to the top of the stack
     */
    void panelPush(final StackAwarePanel sap);

    /**
     * <p>Remove the top-most panel from the internal panel stack.</p>
     *
     * <p>This should result in the top-most panel being removed from the frame, and
     * the next highest element (if any) being displayed.</p>
     *
     * @param exitOnEmpty If {@code true}, the {@link StackAwareFrame} should close if the panel popped was the last element in the stack
     */
    void panelPop(boolean exitOnEmpty);
}
