package com.trexsandwich.interfaces;

import javax.swing.*;

/**
 * <p>Defines the behaviour of a {@link JPanel} that can be managed by a {@link StackAwareFrame}</p>
 *
 * <p>{@link StackAwarePanel#getPanel()}, {@link StackAwarePanel#activate(StackAwareFrame)}, and
 * {@link StackAwarePanel#deactivate(StackAwareFrame)} are the main components required to make this kind of implementation
 * function. The getters/setters are just examples, you can include whatever you need for your own implementation.</p>
 */
public interface StackAwarePanel {
    /**
     * Specify if the host {@link StackAwareFrame} should be resizable when displaying this {@link StackAwarePanel}
     *
     * @return {@code true} if the Frame should be resizable
     */
    boolean isSAPResizable();

    /**
     * Specify the title that the {@link StackAwareFrame} should display when showing this {@link StackAwarePanel}
     *
     * @return Title
     */
    String getSAPTitle();

    /**
     * Provide a reference to the top-level {@link JPanel} that will be added into the {@link StackAwareFrame}. This will likely be {@code this}, as the implementing class will generally extend {@link JPanel}.
     *
     * @return {@link JPanel} to display within the frame
     */
    JPanel getPanel();

    /**
     * Notify {@code this} that it is being displayed, and provide a reference to the {@link StackAwareFrame} that is displaying it.
     *
     * @param host Containing frame
     */
    void activate(StackAwareFrame host);

    /**
     * Notify {@code this} that it is being unloaded from the referenced {@link StackAwareFrame} that is currently displaying it.
     *
     * @param host Containing frame
     */
    void deactivate(StackAwareFrame host);
}
