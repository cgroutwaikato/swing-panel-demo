package com.trexsandwich;

import com.trexsandwich.interfaces.StackAwareFrame;
import com.trexsandwich.interfaces.StackAwarePanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.util.ArrayDeque;
import java.util.Deque;

public class ExampleFrame extends JFrame implements StackAwareFrame {
    /**
     * Internal stack used to track the panels
     */
    private final Deque<StackAwarePanel> panelStack;

    /**
     * Reference to the active panel
     */
    private StackAwarePanel activePanel;

    public ExampleFrame() {
        this.panelStack = new ArrayDeque<>();

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    /**
     * Perform the actions required to load or unload a frame
     */
    private void activateHead() {
        // Clear out any existing content
        this.getContentPane().removeAll();

        // If a panel is active, notify it that it is being unloaded
        if (activePanel != null) {
            activePanel.deactivate(this);
        }

        // Grab the head element from the stack
        activePanel = panelStack.peekFirst();

        // If there is no active panel, bail
        if (activePanel == null) {
            return;
        }

        // Extract params from the head element, updating this with the values
        this.getContentPane().add(activePanel.getPanel());
        this.setMinimumSize(new Dimension(400, 250));
        this.setTitle(activePanel.getSAPTitle());
        this.setResizable(activePanel.isSAPResizable());

        // Notify the head that it is being displayed
        activePanel.activate(this);

        // Pack and redraw
        this.pack();
        this.invalidate();
        this.repaint();
    }

    @Override
    public void panelPush(final StackAwarePanel sap) {
        // Push the new panel to the stack
        panelStack.offerFirst(sap);

        // Activate the topmost panel
        activateHead();
    }

    @Override
    public void panelPop(final boolean exitOnEmpty) {
        // Pop the current head panel from the stack
        final StackAwarePanel sap = panelStack.pollFirst();

        // Activate the topmost panel
        activateHead();

        if (exitOnEmpty && activePanel == null) {
            this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        }

    }
}
